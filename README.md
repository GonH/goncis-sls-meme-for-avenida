Prerequisites:
- serverless framework: `npm install -g serverless`
- aws account
- [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)


# Sections:

## 1: Setup and base lambdas:
Base env and sls config

## 2: Lambda triggers and invokation
Using the base sdk, configuring the account number, setting yml env.
- API Gateway
- Invokation: no trigger