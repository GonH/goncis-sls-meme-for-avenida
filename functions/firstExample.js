const aws = require("aws-sdk");

const handler = async (_event, _context) => {
  try {
    const lambda = new aws.Lambda();

    const secondExampleName = `secondExample-${process.env.STAGE}`;

    const invokationParams = {
      FunctionName: secondExampleName,
      InvocationType: "RequestResponse",
      Payload: JSON.stringify({ message: "My input from js" }),
    };

    const { StatusCode, Payload } = await lambda
      .invoke(invokationParams)
      .promise();

    console.info("[firstExample.handler] Python response:", {
      StatusCode,
      Payload,
    });

    return { statusCode: 200, body: JSON.stringify({ message: `Python said: ${Payload}` }) };
  } catch (error) {
    console.error("[firstExample.handler] Error:", error);

    return "Python failed, what a surprise";
  }
};

module.exports = { handler };
